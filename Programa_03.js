
let numero = 0
console.log("\nRepetição WHILE\n")
while(numero <= 10){
    if(numero == 0){
        console.log(`O número ${numero} é nulo.`)
    } else if(numero % 2 == 0){
        console.log(`O número ${numero} é par.`)
    } else{
        console.log(`O número ${numero} é ímpar.`)
    }
    numero++
}
// // // //
let numero1 = 0
console.log("\nRepetição DO / WHILE\n")
do{
    console.log(`Valor do número: ${numero1}`)
    numero1++
} while(numero1<=10)
// // // //
console.log("\nRepetição FOR\n")
for(let numero2 = 0; numero2 <= 10; numero2++){
    console.log(`Número novo: ${numero2}`)
}